# Proth Primes:

/*
 * Proth numbers are integers of the form:
 * 		p = k * 2^N + 1
 * where 2^N > k.
 * 
 * A Proth prime is a Proth number that is prime.
 * 
 * Proth's theorem states that a Proth number p is
 * prime iff there exists an "a" such that:
 * 		a^((p-1) / 2) == -1  (mod p)		(I)
 * that is, if 
 * 		a^((p-1) / 2) == p-1  (mod p)		(II)
 */



# Compile GTEST:

* mkdir gtest
* cd gtest
* git clone gest-link-here
* mkdir build
* cd build
* mkdir install
* cmake ../ -DCMAKE\_INSTALL\_PREFIX=install/ ../googletest
* make -j4
* make install

# Compile the project

* mkdir build
* cd build
* cmake -DGTEST\_ROOT=path-to-installed-gtest-dir ..

# Known issues:

* The test executable must be run from within the folder where the binary is
  located. This is due to the fact that we're hacking our primality test: it is
  run by a python script, in order to easily support long numbers.

* The primality test algorithm is PSEUDOPRIME from CLRS 3rd edition (page 967),
  which is not ideal. In the future the Miller-Rabin algorithm should be used
  (page 970).

* A naive factorization algorithm is used by the `find_generator` procedure.
  Ideally we would use something slightly better, like Pollard's rho method.

* `find_proth` currently works with N up to 59 without overflowing.

* `find_generator` currently works with N up to 57 without overflowing.
