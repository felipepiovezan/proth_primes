#include "gtest/gtest.h"
#include "proth.hpp"
#include <cstdlib>

TEST(primality_test, is_prime_python_script_is_sane)
{
	//1*2^2 + 1 = 5 is prime
	{
		auto command = std::string("python3.5 is_prime.py ") + std::to_string(2) +
			" " + std::to_string(1);
		auto result = system(command.c_str());
		ASSERT_EQ(result, 0);
	}

	//1*2^3 + 1 = 9 is prime
	{
		auto command = std::string("python3.5 is_prime.py ") + std::to_string(3) +
			" " + std::to_string(1);
		auto result = system(command.c_str());
		ASSERT_NE(result, 0);
	}
}

TEST(proth_primes, all_numbers_generated_are_primes_for_N_up_to_59)
{
	for (auto N = 1; N < 60; N++)
	{
		auto k = proth::find_proth(N);
		ASSERT_NE(k, 0);
		auto command = std::string("python3.5 is_prime.py ") + std::to_string(N) +
			" " + std::to_string(k);
		auto result = system(command.c_str());
		ASSERT_EQ(result, 0);
	}
}

TEST(proth_primes, generators_are_generated_for_N_up_to_59)
{
	for (auto N = 1; N < 60; N++)
	{
		auto k = proth::find_proth(N);
		auto g = proth::find_generator(k, N);
		ASSERT_NE(g, 0) << "N = " << N;
	}
}


TEST(proth_primes, generators_are_actually_generators_for_N_up_to_32)
{
	for (auto N = 1; N < 24; N++)
	{
		auto k = proth::find_proth(N);
		auto p = k * (1 << N) + 1;
		auto g = proth::find_generator(k, N);

		auto x = g;
		for(auto i = 2; i < p-1; i++)
		{
			x = (x * g) % p;
			ASSERT_NE(x, 1) << "g^" << i+1 << " was 1, Shouldn't be 1 before " << p-1;
		}
		x = (x * g) % p;
		ASSERT_EQ(x, 1) << "Failed for p=" << p <<"\ng=" << g;
	}
}

TEST(proth_primes, generators_are_inverted_correctly_up_to_N_59)
{
	for (auto N = 1; N < 60; N++)
	{
		auto k = proth::find_proth(N);
		unsigned __int128 one = 1;
		unsigned __int128  p = k * (one << N) + 1;
		unsigned __int128  g = proth::find_generator(k, N);
		unsigned __int128  g_inv = proth::invert_generator(g, p);
		unsigned __int128 expected_one = (g * g_inv) % p;
		ASSERT_EQ(expected_one, 1) << "Failed at iteration = " << N;
	} 
}

int main(int argc, char** argv)
{
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}
