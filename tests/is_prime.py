import sys

def compute_proth(n, k):
    proth = (2**n)*k + 1
    return proth

def isprime(x):
    remainder = pow(2, x-1, x)
    return remainder == 1

if __name__ == '__main__':
    if len(sys.argv) != 3:
        print("Wrong number of arguments")
        print("Usage: python is_prime.py N K")
        sys.exit(2)

    N = int(sys.argv[1])
    k = int(sys.argv[2])
    p = compute_proth(N, k)
    
    is_prime = isprime(p)
    if is_prime:
        sys.exit(0)
    else:
        sys.exit(1)


