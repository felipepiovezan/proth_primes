#include "proth.hpp"

namespace
{
uint64_t modular_exp(uint64_t base_, uint64_t exponent_, uint64_t mod_)
{
	unsigned __int128 base = base_;
	unsigned __int128 exponent = exponent_;
	unsigned __int128 mod = mod_;

	unsigned __int128 d = 1;
	for(int i = sizeof(__int128)*8 - 1 ; i >= 0; --i)
	{
		__int128 digit = (exponent >> i) & 1;
		d = (d * d) % mod;
		if (digit)
		{
			d = (d * base) % mod;
		}
	}

	return d;
}


std::vector<uint32_t> get_prime_factors(uint32_t n)
{
	auto original = n;
	std::vector<uint32_t> ans;

	for(uint32_t div = 2; div <= original; div++)
	{
		if(n % div == 0)
		{
			ans.push_back(div);
			do
			{
				n = n/div;
			}
			while(n % div == 0);
		}
	}

	if (n != 1)
	{
		std::cout << original << ", " << n << std::endl;
		assert(n == original);
		ans.push_back(n);
	}

	return ans;
}
} // unnamed namespace;


uint64_t proth::find_generator(uint64_t k, uint64_t N)
{
	uint64_t exp = 1;
	exp = exp << N;
	uint64_t p = k * exp + 1;
	std::vector<uint32_t> factors = get_prime_factors(k);
	factors.push_back(2);
	for(uint64_t g = 2; g < p; g++)
	{
		bool is_gen = true;
		for(auto factor : factors)
		{
			if( modular_exp(g, (p-1)/factor, p) == 1)
			{
				is_gen = false;
				break;
			}
		}
		if (is_gen)
			return g;
	}
	
	return 0;
}

uint64_t proth::invert_generator(uint64_t g, uint64_t p)
{
	return modular_exp(g, p-2, p);
}

uint64_t proth::find_proth(uint64_t N)
{
	uint64_t exp = 1;
	exp = exp << N;
	for (uint64_t k = 1; k <= 1024; k ++)
	{
		uint64_t p = k * exp + 1;
		uint64_t exp_div = (p - 1)/2;
		for (uint64_t a = 1; a <= 1024; a++)
		{
			auto modulo = modular_exp(a, exp_div, p);
			if (modulo == p - 1)
				return k;
		}
	}
	
	return 0;
}

