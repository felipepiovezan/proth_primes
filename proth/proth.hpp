#ifndef _PROTH_HPP_
#define _PROTH_HPP_

#include <iostream>
#include <cstdint>
#include <cassert>
#include <vector>

/*
 * Proth numbers are integers of the form:
 * 		p = k * 2^N + 1
 * where 2^N > k.
 * 
 * A Proth prime is a Proth number that is prime.
 * 
 * Proth's theorem states that a Proth number p is
 * prime iff there exists an "a" such that:
 * 		a^((p-1) / 2) == -1  (mod p)		(I)
 * that is, if 
 * 		a^((p-1) / 2) == p-1  (mod p)		(II)
 */

/*
 * This program is responsible for:
 * 1) Given N, find a K such that
 * 		p = k * 2^N + 1
 * is a Proth prime
 * 2) Given a pair (N,k) representing a Proth prime,
 * find a generator g for Z_p*.
 */

namespace proth
{
	/*
	 * Returns a number k such that (n, K) represents
	 * a Proth prime.
	 * Remarks:
	 * 		- For values of 'k' up to 1024, it performs 
	 * 		the test (II) on values of 'a' in the range
	 * 		[1; 1024]. 'k' is incremented if no 'a' in
	 * 		this range satisfies (II). An assertion
	 * 		fails if a k in this range is not found.
	 * 		- Works for values N < 60
	 */
	uint64_t find_proth(uint64_t N);

	/*
	 * Returns a generator for the multiplicative group
	 * Z_p*.
	 * Remarks:
	 * 		- It uses a naive method of factoring k into
	 * 		its prime divisors, therefore k shouldn't be
	 * 		too big. Performs O(k) operations for this.
	 * 		- The total runtime depends on the distribution
	 * 		of generators on this group. More mathematical
	 * 		background is needed here.
	 * 		- Works for values N < 60
	 */
	uint64_t find_generator(uint64_t k, uint64_t N);

	/*
	 * Inverts a given generator g for Z_p.
	 */
	uint64_t invert_generator(uint64_t g, uint64_t p);
}


#endif
